# Access Control Point API Implementation

This project is an implementation in Silex/PHP of the ACP API defined in the RISIS Project.
It allows you to generate a new API from an existing database or from a dataset.


## Prerequisites

* Git : https://git-scm.com/
* Composer : https://getcomposer.org/
* A Web server, like Apache : https://httpd.apache.org/
* MySQL : http://www-fr.mysql.com/


## Installation

Choose one of the following methods to install the project:

### Option 1: Using docker-compose
1. `git clone` the project
2. Build docker image: `docker build -t access-control-point .`
3. Create two new tables in your database: `dictionnary` and `entities`, and some some views if needed. (See more details in the [Database Model](#database-model) section below).
4. Create a configuration file `parameters-<your-db-name>.json` from [parameters.json](config/parameters.json) in the [config](config/) directory. (See more details in the [Configuration](#configuration) section below).
5. Create a `docker-compose-<your-db-name>.yml` from [docker-compose.yml](docker-compose.yml) with one service for the API, and another for the database in case it does not exist already and you want to create it. (See more details in the [Database Source](#database-source) section below).
6. Deploy it: `docker-compose -f access-control-point/docker-compose.yml up -d`

### Option 2: Old way
1. First git clone the project
2. Make your Web server point on web directory of the project
3. Install dependecies by running composer in the root directory of the project
composer update
4. Create configuration file parameters.json from parameters.json.dist in config directory
5. Execute bdd_acp.sql sql script on the database of the project
6. Populate your DB with data. To make experiments, a sample of dummy data is available in the directory scripts_sql. Play the acp_dummy_data.sql script on your database to add his content.


## Configuration

The file [parameters.json](config/parameters.json) contains a default configuration. You must copy its content and adapt the configuration to your dataset.

* debug : true/false, to activate debugging information from silex
* monolog : configuration of the log file
* db.options : Database access parameters
  * default_limit : The default limit (number of records) for queries that retrieve multiple instances of an entity
  * max_limit : The maximum limit (number of records) for queries that retrieve multiple instances of an entity, even if the limit value in the request is greater.
* metadata : Meta information about the dataset
* trustedProxies : Not yet used
* accessToken : An object that contains properties like "owner":"token". You can define here access Token. The key name (owner) just here to remember who is the owner of the token. To permit access without token, add a property with an empty token.


## Database Source

#### Existing database
- Include the configuration of the existing database in the `parameters-<your-db-name>.json` file.
- In the `docker-compose-<your-db-name>.yml` file only include one service for the API. If you are using the [template](docker-compose.yml) remove the `depends_on` option.

#### Dataset file
- Create a file `<your-db>-initdb.sql` like [acp_dummy_data.sql](scripts_sql/acp_dummy_data.sql). You define there the queries to perform in your database. 
  - You can also load files into tables using the `LOAD DATA INFILE` function in the `sql` script. This is an example:
    ```sql
    LOAD DATA INFILE '/tmp/acp_dummy/countries_info.csv'
    INTO TABLE Country
    CHARACTER SET utf8
    FIELDS TERMINATED BY ',' ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 ROWS;
    ```
    Just make sure the location where you are getting the data from, is available in the db service created in the `docker-compose` file (see section [Option 1: Using docker-compose](#option-1-using-docker-compose)):
    ```yml
    services:
      ...
      your-db:
        ...
        volumes:
          ...
          - ./tmp/acp_dummy:/tmp/acp_dummy
        ..
    ```

- In the `docker-compose-<your-db-name>.yml` file include two services, one to create the new DB and another for the API (just like in the [template](docker-compose.yml)). Make sure the volumes correspond to the files that will be used.
- Include the configuration of the new database in the `parameters-<your-db-name>.json` file. This is what you set in the previous step.


## Database Model

Two tables are needed for metadata in your database:

* `dictionnary` table: for each table, contains the list of fields and their description
  * entityType: the table name
  * name: the name of the field
  * description: the description of the field
  * hasEntityType: if the field defines a relation and his content is another entity, then hasEntityType defines the related entityType, otherwise this field is null

* `entities` table: contains the description of available entitities
  * entity: the tablename as defined in the parameters file
  * description: the description of the entity
  * active: 1 if you want this table (entity) to be available in the API that will generate access-control-point, otherwise 0

You can create views in your database if you plan to implement the `/search` functionality to your API. 


## Model example

For example, if you have two entity type : Person and Institution

- `dictionnary`  
  | entityType  | name        | description                          | hasEntityType |
  |-------------|-------------|--------------------------------------|---------------|
  | Person      | name        | Person name                          | NULL          |
  | Person      | institution | Institution where the person belongs | Institution   |
  | Institution | name        | Institution name                     | NULL          |

- `entities`
  | entity      | description                                        | active |
  |-------------|----------------------------------------------------|--------|
  | Person      | Persons that belong or belonged to any Institution | 1      |
  | Institution | Available institutions                             | 1      |

- `Person`
  | id    | name    | institution |
  |-------|---------|-------------|
  | 124AZ | Gandalf | TS564       |

- `Institution`
  | id    | name      |
  |-------|-----------|
  | TS564 | The Shire |


## Usage

You can currently use `v1.1` and `v1.2` simultaneously.

### `v1.1`

- Get metadata about the dataset:

  `http://my-db.risis.localhost/v1.1/metadata`

- Get the list of available entity types:

  `http://my-db.risis.localhost/v1.1/entityTypes`

- Get all instances of an entity (in the limit of max_limit records):

  `http://my-db.risis.localhost/v1.1/entities/<entity-name>`

- Filter any instance of an entity by an attribute in different ways:
  
  - One value:  
    `http://my-db.risis.localhost/v1.1/entities/<entity-name>/<attribute-1>=<value-1>`  
    Example: `http://patents.risis.localhost/v1.1/entities/patents/granted_year=2001`
  
  - Multiple values (as many as you want, separated by comma):  
    `http://my-db.risis.localhost/v1.1/entities/<entity-name>/<attribute-1>.in=<value-1>,<value-2>,<value-3>`  
    Example: `http://patents.risis.localhost/v1.1/entities/patents/granted_year.in=2001,2008,2009`
  
  - Range values (two values, separated by comma):  
    `http://my-db.risis.localhost/v1.1/entities/<entity-name>/<attribute-1>.between=<value-1>,<value-2>`  
    Example: `http://patents.risis.localhost/v1.1/entities/patents/granted_year.between=2001,2008`

You can always use the `limit` and `offset` query params if you want.

### `v1.2`

This version allows a more complex filtering of the data through the API, using a `POST` method to send the parameters of the filtering in a `json` body. 
  - `query`: **required** parameter to specify the filtering conditions.
  - `divideFromTable`: **optional** parameter to specify the name of one table you want to fetch data from. If not specified, the data will be fetched from the database view specified in the configuration (`parameters-<your-db-name>.json` -> `metadata` -> `query_builder_view_name`).

`POST`: `http://my-db.risis.localhost/v1.2/search`

You can use the `limit` in the query params if you want.  
See more details about the `search` criteria in a [section below](#search-criteria).

#### Example
From the patents database:
- Filter patents classified with `ipc_class` = `F04D  15/00` OR `F04D  13/06` AND whose applicant's country is Germany (`iso_country` = `DE`)
- Fetch only the information of the patent applicants `"divideFromTable": "applicants"`

`POST`: `http://patents.risis.localhost/v1.2/search`  

request body:  
```json
{
"query": {
  "operatorIdentifier": "AND",
  "children": [
    {
      "operatorIdentifier": "OR",
      "children": [
        {
          "identifier": "tech_classifications.ipc_class",
          "value": {
            "type": "text",
            "data": "F04D  15/00",
            "condition": {
              "id": "equal",
              "name": "Equal",
              "humanOp": "=",
              "isNotOp": false,
              "cardinality": 1
            },
            "isDataCompleted": true
          }
        },
        {
          "identifier": "tech_classifications.ipc_class",
          "value": {
            "type": "text",
            "data": "F04D  13/06",
            "condition": {
              "id": "equal",
              "name": "Equal",
              "humanOp": "=",
              "isNotOp": false,
              "cardinality": 1
            },
            "isDataCompleted": true
          }
        }
      ]
    },
    {
      "identifier": "applicants.iso_country",
      "value": {
        "type": "text",
        "data": "DE",
        "condition": {
          "id": "equal",
          "name": "Equal",
          "humanOp": "=",
          "isNotOp": false,
          "cardinality": 1
        },
        "isDataCompleted": true
      }
    }
  ]
},
"divideFromTable": "applicants"
}
```


## Search criteria
The search criterion defines the conditions that must be met for an object to be returned by a search query. The criterion consists of a set of logical rules.

### Logical Operators `operatorIdentifier`

- `and`
- `or`
- `not`: (`not`, `not ... and`, `not ... or`)

### Identifiers `identifier`

Identifiers MUST have the form `[entity.]field`

- `entity` refers to the type or category of data that the field belongs to. For example, a specific table in your database.
- `field` refers to a specific field belonging to that `entity` (or table).

### Types `value.type`

```
boolean
date
datetime
multiselect
number
select
slider
text
textarea
time
```

### Conditions `value.condition`

```
equal
not_equal
less
less_or_equal
greater
greater_or_equal
like
not_like
starts_with
ends_with
between
not_between
is_empty
is_not_empty
is_null
is_not_null
select_equals
select_not_equals
select_any_in
select_not_any_in
```

See a JS definition of all the available conditions here: [searchConditions.js](searchConditions.js).

## Release History

v1.0 - 2016/10/01 First version

v1.1 - 2016/10/12 v1.1 of API

v1.1 - 2022/08/24 Update API to filter based on query params

v1.2 - 2023/01/24 v1.2 API search


## Contributors

- [Guillaume Orsal - Développeur Web](https://www.orsal.fr "CV Ingénieur informatique indépendant") at [Cortext Platform](http://www.cortext.net) / [INRA](http://www.inra.fr) / [LISIS](http://www.umr-lisis.org) / [RISIS](http://risis.eu)
- RCF team

## License

- Copyright (c) 2016 Guillaume Orsal
- Copyright (c) 2019 Philippe Breucker
- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.
