<?php
/* Access Control Point Functions for RCF Query Builder */

use Silex\Application;

function queryIdentifierDecorator( $identifier ) {
  list($field, $entity, $source) = array_reverse(explode('.', $identifier));
  if ( empty($entity) ) {
    throw new Exception('invalid identifier format');
  }
  return "`{$entity}.{$field}`";
}

/**
 * Return results of SQL query execution
 */
function runSqlQuery( Application $app, String $sqlQuery ) {
  if ( isset($sqlQuery) && is_string($sqlQuery) && !empty($sqlQuery) ) {
    return $app['db']->fetchAll($sqlQuery);
  }
  else {
    throw new Exception('invalid query');
  }
}

function convertToSQL($condition) {
  $operatorIdentifier = $condition['operatorIdentifier'];
  $children = $condition['children'];
  if ($operatorIdentifier === 'NOT_AND' || $operatorIdentifier === 'NOT_OR') {
    $operator = $operatorIdentifier === 'NOT_AND' ? 'AND' : 'OR';
    $notOperator = "NOT";
  } else {
    $operator = $operatorIdentifier;
    $notOperator = "";
  }
  $conditions = array();
  foreach ($children as $child) {
    if(isset($child['children'])) {
      $conditions[] = convertToSQL($child);
    } else {
      $conditions[] = buildConditionString($child);
    }
  }
  $conditionString = implode(" $operator ", $conditions);
  return "$notOperator ($conditionString)";
}

function buildConditionString($child) {
  $identifier = queryIdentifierDecorator($child['identifier']);
  $value = $child['value']['data'];
  $conditionId = $child['value']['condition']['id'];
  switch ($conditionId) {
    case 'between':
      $values = explode(",", $child['value']['data']);
      $a = $values[0];
      $b = $values[1];
      return "$identifier BETWEEN $a AND $b";
    case 'ends_with':
      return "$identifier LIKE CONCAT('%', '$value')";
    case 'equal':
      return "$identifier = '$value'";
    case 'greater':
      return "$identifier > $value";
    case 'greater_or_equal':
      return "$identifier >= $value";
    case 'is_empty':
      return "$identifier = ''";
    case 'is_not_empty':
      return "$identifier != ''";
    case 'is_not_null':
      return "$identifier IS NOT NULL";
    case 'is_null':
      return "$identifier IS NULL";
    case 'less':
      return "$identifier < $value";
    case 'less_or_equal':
      return "$identifier <= $value";
    case 'like':
      return "$identifier LIKE '%$value%'";
    case 'not_between':
      $values = explode(",", $child['value']['data']);
      $a = $values[0];
      $b = $values[1];
      return "$identifier NOT BETWEEN $a AND $b";
    case 'not_equal':
      return "$identifier != '$value'";
    case 'not_like':
      return "$identifier NOT LIKE '%$value%'";
    case 'select_any_in':
      $valueArray = implode("','", $value);
      return "$identifier IN ('$valueArray')";
    case 'select_equals':
      return "$identifier = '$value'";
    case 'select_not_any_in':
      $valueArray = implode("','", $value);
      return "$identifier NOT IN ('$valueArray')";
    case 'select_not_equals':
      return "$identifier != '$value'";
    case 'starts_with':
      return "$identifier LIKE '$value%'";
    default:
      return '';
  }
}

function queryToSQL( $app, $target, $query, $offset, $limit, $divideFromTable) {
  $sqlQuery = '';
  $selectData = '*';
  if ( $divideFromTable ) {
    $sql_query = "DESCRIBE $target";
    $result = $app['db']->fetchAll($sql_query);
    $column_names = array();
    foreach ($result as $row) {
      if (strpos($row['Field'], $divideFromTable . ".") === 0) {
        $column_names[] = $row['Field'];
      }
    }
    $column_names = array_map(fn($i) => "`$i`", $column_names);
    $selectData = "DISTINCT " . implode(', ', $column_names);
  }
  $sqlQuery = "SELECT ". $selectData ." FROM $target ";
  $sqlConditions = convertToSQL($query);
  $sqlQuery = $sqlConditions ? $sqlQuery . " WHERE " . $sqlConditions : $sqlQuery;
  $sqlQuery = $sqlQuery . " LIMIT $offset,$limit";
  return $sqlQuery;
}

function buildCreateViewSqlQuery( Application $app ) {
  $dictionary = $app['db']->fetchAll('SELECT * FROM dictionnary');
  $entities = $app['db']->fetchAll('SELECT entity FROM entities WHERE active > 0');

  $viewName = implode('_', array_map(fn($e) => $e['entity'], $entities));

  $viewColumns = array_map(
    fn($i) => "{$i['entityType']}.{$i['name']} AS '"
      . strtolower($i['entityType'])
      . '.'
      . strtolower($i['name'])
      . "'",
    $dictionary);

  $viewJoins = array_map( fn($i) => $i['entity'], $entities );

  $sqlQuery = 'CREATE VIEW '
    . $viewName
    . ' AS SELECT '
    . implode(', ', $viewColumns)
    . ' FROM '
    . implode(' INNER JOIN ', $viewJoins);

  return $sqlQuery;
}
