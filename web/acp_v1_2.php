<?php
/* Access Control Point */
/* V1.2 of the ACP API */

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Silex\Application\UrlGeneratorTrait;

// Initialisation of controller
$acp = $app['controllers_factory'];

require_once 'acp_query_builder.php';

/**
 * Query the database by parsing RCF Query Factory JSON format
 */
$acp->post('/search', function(Application $app, Request $request) {
    // Log action
    $app['monolog']->addInfo("Search");
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
      $body = json_decode($request->getContent(), true);
      if (isset($body)) {
        $query = $body['query'];
        if (isset($query)) {
          $target = $body['query_builder_view_name'];
          if(empty($target)) {
            $target = $app['parameters']['metadata']['query_builder_view_name'];
          }
          if ( $target ) {
            $offset = (int)$request->get('offset');
            $limit = (int)$request->get('limit');
            if ( empty($limit) || $limit == 0 )
              $limit = $app['parameters']['db.options']['default_limit'];
            $limit = min( $limit, $app['parameters']['db.options']['max_limit'] );
              try {
              $sqlQuery = queryToSQL($app, $target, $query, $offset, $limit, $body['divideFromTable'] );
              $response = array();
              if ( $app['APP_ENV'] === 'development' && $app['debug']) {
                $response['debug'] = array(
                  'sqlQuery' => $sqlQuery,
                  'sqlView' => buildCreateViewSqlQuery( $app )
                );
              }
              $results = runSqlQuery( $app, $sqlQuery );
              $response['results'] = $results;
              $response['offset'] = $offset;
              $response['limit'] = $limit;
              return $app->json($response);
            }
            catch ( Exception $e ) {
              $response['error'] = array(
                'code' => 7,
                'message' => $e->getMessage()
              );
              return $app->json($response, 400);
            }
          }
          else {
            $error["code"] = 8;
            $error["message"] = "parameter 'query_builder_view_name' is empty";
            return $app->json(array('error' => $error), 400);
          }
        }
        else {
          $error["code"] = 6;
          $error["message"] = "query missing";
          return $app->json(array('error' => $error), 400);
        }
      }
      else {
        $error["code"] = 5;
        $error["message"] = "empty request";
        return $app->json(array('error' => $error), 400);
      }
    }
    else {
      $error["code"] = 4;
      $error["message"] = "unexpected content-type";
      return $app->json(array('error' => $error), 400);
    }
});

return $acp;
