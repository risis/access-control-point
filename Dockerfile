FROM php:7-apache-buster AS base

RUN apt update
RUN apt --assume-yes install git zip unzip
RUN a2enmod rewrite
RUN docker-php-ext-install pdo_mysql

COPY . /acp
WORKDIR /acp

RUN COMPOSER_ALLOW_SUPERUSER=1 curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.10.25
RUN composer update

ENV APACHE_DOCUMENT_ROOT /acp/web
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

FROM base AS dev
RUN pecl install xdebug-2.9.0 && docker-php-ext-enable xdebug
COPY config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

FROM base AS prod